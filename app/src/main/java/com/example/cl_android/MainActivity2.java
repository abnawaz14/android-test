package com.example.cl_android;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
//Guidelines(margin,divide screen),Barrier(dynamically provide space),Chain (chaining groups),Group( only for visible,invisible,gone )
public class MainActivity2 extends AppCompatActivity {


   @BindView(R.id.button4) Button btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        ButterKnife.bind(this);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) btn.getLayoutParams();
                layoutParams.width = 450;
                btn.setLayoutParams(layoutParams);

//                Toast.makeText(MainActivity2.this, "OK", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void clickMe(View view) {

        Intent i=new Intent(MainActivity2.this,MainActivity.class);
        startActivity(i);
    }
}