package com.example.cl_android;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.ConstraintSet;
import androidx.transition.TransitionManager;
import androidx.transition.AutoTransition;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    private ConstraintSet constraintSet1 = new ConstraintSet();
    private ConstraintSet constraintSet2 = new ConstraintSet();
    private ConstraintLayout constraintLayout;
    private Button departButton;
    @BindView(R.id.rocketIcon) ImageView rocketIcon;

    private Boolean isOffscreen = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.keyframe1);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        constraintLayout=findViewById(R.id.constraintLayout); //keyframe
        departButton=findViewById(R.id.departButton);
        constraintSet1.clone(constraintLayout); //keyframe
        constraintSet2.clone(this, R.layout.activity_main); //main
//        departButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                TransitionManager.beginDelayedTransition(constraintLayout);
//                ConstraintSet constraint = (!isOffscreen) ? constraintSet1 : constraintSet2;
//                        isOffscreen = !isOffscreen;
//                constraint.applyTo(constraintLayout);
//            }
//        });
        departButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) rocketIcon.getLayoutParams();
                float startAngle = layoutParams.circleAngle;
                float endAngle = startAngle + 360 ;

                //2
                ValueAnimator anim = ValueAnimator.ofFloat(startAngle, endAngle);
                anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        Float animatedValue =(Float) animation.getAnimatedValue();
                        ConstraintLayout.LayoutParams layoutParams = (ConstraintLayout.LayoutParams) rocketIcon.getLayoutParams();
                        layoutParams.circleAngle = animatedValue;
                        rocketIcon.setLayoutParams(layoutParams);


                        rocketIcon.setRotation((animatedValue % 360 - 270));
                    }
                });

                anim.setDuration(2000);


                anim.setInterpolator(new LinearInterpolator());
                anim.start();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    @Override
    public void onEnterAnimationComplete() {
        super.onEnterAnimationComplete();
        constraintSet2.clone(this, R.layout.activity_main);

        //apply the transition
        AutoTransition transition = new AutoTransition();
        transition.setDuration(1000);
        TransitionManager.beginDelayedTransition(constraintLayout, transition);

        constraintSet2.applyTo(constraintLayout);
    }
}